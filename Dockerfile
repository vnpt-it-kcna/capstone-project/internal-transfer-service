FROM gcr.io/distroless/java17-debian11
WORKDIR /project
COPY build/libs/app.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]