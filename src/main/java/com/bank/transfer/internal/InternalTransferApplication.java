package com.bank.transfer.internal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternalTransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(InternalTransferApplication.class, args);
	}

}
