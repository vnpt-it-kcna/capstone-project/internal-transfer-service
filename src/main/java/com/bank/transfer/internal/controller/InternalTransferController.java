package com.bank.transfer.internal.controller;

import com.bank.transfer.internal.model.TransferRequest;
import com.bank.transfer.internal.util.ObjectUtil;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class InternalTransferController {
    @Autowired
    private ProducerTemplate template;

    @PostMapping(value = "/v1/internal-transfer/init", produces = "application/json")
    @Operation(summary = "Init internal transfer")
    public ResponseEntity<?> tranfer(
            @Valid @RequestBody TransferRequest transferRequest) {
        Map<String, String> transferHeader = new HashMap<>();
        transferHeader.put("transfer_id", transferRequest.getClientTransactionId().toString());
        log.info("Transfer request: " + ObjectUtil.objectToString(transferRequest));
        String router = template.requestBodyAndHeader("direct:internal_tranfer_router",
                transferRequest,
                "transfer_id",
                transferRequest.getClientTransactionId()).toString();
        //template.requestBody("direct:apiTest", transferRequest).toString();
        return ResponseEntity.ok(router);
    }
}
